"use strict";

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.get("/", () => {
    return { greeting: "Hello world in JSON" };
});

Route.get("bordas", "BordaController.index");
Route.get("tamanhos", "TamanhoController.index");
Route.get("massas", "MassaController.index");
Route.get("recheios", "RecheioController.index");

Route.post("pedido", "PizzaController.pedido");
