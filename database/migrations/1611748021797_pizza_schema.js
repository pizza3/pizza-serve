"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class PizzaSchema extends Schema {
    up() {
        this.create("pizzas", (table) => {
            table.increments();
            table
                .integer("tamanho_id")
                .unsigned()
                .references("id")
                .inTable("tamanhos")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table
                .integer("massa_id")
                .unsigned()
                .references("id")
                .inTable("massas")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table
                .integer("borda_id")
                .unsigned()
                .references("id")
                .inTable("bordas")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");
            table
                .integer("recheio_id")
                .unsigned()
                .references("id")
                .inTable("recheios")
                .onDelete("CASCADE")
                .onUpdate("CASCADE");

            table.timestamps();
        });
    }

    down() {
        this.drop("pizzas");
    }
}

module.exports = PizzaSchema;
