"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class TamanhoSchema extends Schema {
    up() {
        this.create("tamanhos", (table) => {
            table.increments();
            table.string("name", 32).notNullable();
            table.float("price", 2).notNullable();
            table.integer("score").notNullable();
            table.integer("promotion_day_start").notNullable();
            table.integer("promotion_day_end").notNullable();
            table.timestamps();
        });
    }

    down() {
        this.drop("tamanhos");
    }
}

module.exports = TamanhoSchema;
