"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class BordaSchema extends Schema {
    up() {
        this.create("bordas", (table) => {
            table.increments();
            table.string("name", 32).notNullable();
            table.float("price", 2).notNullable();
            table.integer("score").notNullable();
            table.integer("promotion_day_start").notNullable();
            table.integer("promotion_day_end").notNullable();
            table.timestamps();
        });
    }

    down() {
        this.drop("bordas");
    }
}

module.exports = BordaSchema;
