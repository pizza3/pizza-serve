"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class RecheioSchema extends Schema {
    up() {
        this.create("recheios", (table) => {
            table.increments();
            table.string("name", 32).notNullable();
            table.string("description", 128).notNullable();
            table.float("price", 2).notNullable();
            table.integer("score").notNullable();
            table.integer("promotion_day_start").notNullable();
            table.integer("promotion_day_end").notNullable();
            table.timestamps();
        });
    }

    down() {
        this.drop("recheios");
    }
}

module.exports = RecheioSchema;
