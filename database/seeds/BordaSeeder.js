"use strict";

/*
|--------------------------------------------------------------------------
| BordaSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class BordaSeeder {
    async run() {
        await Factory.get("bordas").create({
            name: "Borda tradicional",
            price: 0.0,
            promotion_day_start: 0,
            promotion_day_end: 2,
        });
        await Factory.get("bordas").create({
            name: "Borda de catupiry",
            price: 6.5,
            promotion_day_start: 3,
            promotion_day_end: 4,
        });
        await Factory.get("bordas").create({
            name: "Borda de cheddar",
            price: 7.0,
            promotion_day_start: 3,
            promotion_day_end: 6,
        });
    }
}

module.exports = BordaSeeder;
