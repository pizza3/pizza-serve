"use strict";

/*
|--------------------------------------------------------------------------
| RecheioSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class RecheioSeeder {
    async run() {
        await Factory.get("recheios").create({
            name: "Calabresa",
            description: "Molho de tomate, mussarela, calabresa e orégano",
            price: 43.0,
            promotion_day_start: 0,
            promotion_day_end: 0,
        });
        await Factory.get("recheios").create({
            name: "Humita",
            description: "Mussarela, milho e orégano",
            price: 43.0,
            promotion_day_start: 0,
            promotion_day_end: 0,
        });
        await Factory.get("recheios").create({
            name: "Mista",
            description:
                "Molho de tomate, mussarela, apresuntado, calabresa e orégano",
            price: 40.0,
            promotion_day_start: 0,
            promotion_day_end: 0,
        });
        await Factory.get("recheios").create({
            name: "Napolitana",
            description:
                "Molho de tomate, mussarela, tomate, parmesão e orégano",
            price: 43.0,
            promotion_day_start: 0,
            promotion_day_end: 0,
        });
        await Factory.get("recheios").create({
            name: "Portuguesa",
            description:
                "Molho de tomate, mussarela, apresuntado, calabresa, ovo, pimentão, cebola, azeitona e orégano",
            price: 43.0,
            promotion_day_start: 1,
            promotion_day_end: 1,
        });
        await Factory.get("recheios").create({
            name: "Apresuntado",
            description: "Molho de tomate, mussarela, apresuntado e orégano",
            price: 43.0,
            promotion_day_start: 1,
            promotion_day_end: 1,
        });
        await Factory.get("recheios").create({
            name: "Italiana",
            description:
                "Molho de tomate, mussarela, apresuntado, milho, catupiry e orégano",
            price: 43.0,
            promotion_day_start: 1,
            promotion_day_end: 1,
        });
        await Factory.get("recheios").create({
            name: "Mussarela",
            description: "Mussarela e orégano",
            price: 43.0,
            promotion_day_start: 1,
            promotion_day_end: 1,
        });
        await Factory.get("recheios").create({
            name: "Montes CLAROS",
            description:
                "Molho de tomate, mussarela, apresuntado, milho, cebola, pimentão, tomate e orégano",
            price: 43.0,
            promotion_day_start: 2,
            promotion_day_end: 2,
        });
        await Factory.get("recheios").create({
            name: "A moda",
            description:
                "Molho de tomate, mussarela, calabresa, apresuntado, cebola, pimentão, tomate e orégano",
            price: 43.0,
            promotion_day_start: 2,
            promotion_day_end: 2,
        });
        await Factory.get("recheios").create({
            name: "Marguerita",
            description:
                "Molho de tomate, mussarela, tomate, manjericão seco e orégano",
            price: 43.0,
            promotion_day_start: 2,
            promotion_day_end: 2,
        });
        await Factory.get("recheios").create({
            name: "Alho",
            description: "Molho de tomate, mussarela, alho, cebola e orégano",
            price: 43.0,
            promotion_day_start: 2,
            promotion_day_end: 2,
        });
        await Factory.get("recheios").create({
            name: "Du cheff",
            description:
                "Molho de tomate, mussarela, milho, bacon, calabresa, cebola, azeitona e orégano",
            price: 46.0,
            promotion_day_start: 3,
            promotion_day_end: 3,
        });
        await Factory.get("recheios").create({
            name: "Vegetariana",
            description:
                "Molho de tomate, mussarela, palmito, milho verde, tomate, ervilha, azeitona e orégano",
            price: 46.0,
            promotion_day_start: 3,
            promotion_day_end: 3,
        });
        await Factory.get("recheios").create({
            name: "Atum",
            description: "Molho de tomate, mussarela, atum e orégano",
            price: 46.0,
            promotion_day_start: 3,
            promotion_day_end: 3,
        });
        await Factory.get("recheios").create({
            name: "Bacon",
            description: "Molho de tomate, mussarela, bacon e orégano",
            price: 46.0,
            promotion_day_start: 3,
            promotion_day_end: 3,
        });
        await Factory.get("recheios").create({
            name: "Da casa",
            description:
                "Molho de tomate, mussarela, calabresa, milho, ervilha, ovos, tomate e orégano",
            price: 46.0,
            promotion_day_start: 4,
            promotion_day_end: 4,
        });
        await Factory.get("recheios").create({
            name: "Frango a bolonhesa",
            description:
                "Molho bolonhesa, mussarela, frango desfiado e orégano",
            price: 46.0,
            promotion_day_start: 4,
            promotion_day_end: 4,
        });
        await Factory.get("recheios").create({
            name: "Frango catupiry",
            description:
                "Molho de tomate, mussarela, frango, catupiry, orégano",
            price: 46.0,
            promotion_day_start: 4,
            promotion_day_end: 4,
        });
        await Factory.get("recheios").create({
            name: "Carijó",
            description:
                "Molho de tomate, mussarela, frango, milho, requeijão tipo catupiry e orégano",
            price: 41.0,
            promotion_day_start: 4,
            promotion_day_end: 4,
        });
        await Factory.get("recheios").create({
            name: "Caipira",
            description:
                "Molho de tomate, mussarela, frango, calabresa, milho e orégano",
            price: 51.0,
            promotion_day_start: 5,
            promotion_day_end: 5,
        });
        await Factory.get("recheios").create({
            name: "Quatro queijos",
            description:
                "Molho de tomate, mussarela, catupiry, parmesão, provolone, orégano",
            price: 46.0,
            promotion_day_start: 5,
            promotion_day_end: 5,
        });
        await Factory.get("recheios").create({
            name: "Especial",
            description:
                "Molho de tomate, mussarela, calabresa, milho, palmito, frango, bacon, azeitona, tomate, cebola e pimentão",
            price: 44.0,
            promotion_day_start: 5,
            promotion_day_end: 5,
        });
        await Factory.get("recheios").create({
            name: "Peito de peru à francesa",
            description:
                "Mussarela, molho especial, peito de peru, creme de leite e orégano",
            price: 51.0,
            promotion_day_start: 5,
            promotion_day_end: 5,
        });
        await Factory.get("recheios").create({
            name: "Real pizza",
            description:
                "Molho de tomate, mussarela, presunto, calabresa, frango, palmito, milho, cebola, tomate, pimentão, bacon, azeitona e orégano",
            price: 51.0,
            promotion_day_start: 6,
            promotion_day_end: 6,
        });
        await Factory.get("recheios").create({
            name: "Lombo à francesa",
            description:
                "Molho de tomate, mussarela, lombo defumado, creme de leite e orégano",
            price: 48.0,
            promotion_day_start: 6,
            promotion_day_end: 6,
        });
        await Factory.get("recheios").create({
            name: "Palmito",
            description: "molho de tomate, mussarela e palmito",
            price: 43.0,
            promotion_day_start: 6,
            promotion_day_end: 6,
        });
    }
}

module.exports = RecheioSeeder;
