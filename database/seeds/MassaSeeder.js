"use strict";

/*
|--------------------------------------------------------------------------
| MassaSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class MassaSeeder {
    async run() {
        await Factory.get("massas").create({
            name: "Massa Tradicional",
            price: 0.0,
            promotion_day_start: 0,
            promotion_day_end: 2,
        });
        await Factory.get("massas").create({
            name: "Massa nova-iorquina",
            price: 7.5,
            promotion_day_start: 2,
            promotion_day_end: 4,
        });
        await Factory.get("massas").create({
            name: "Massa siciliana",
            price: 10.5,
            promotion_day_start: 3,
            promotion_day_end: 6,
        });
    }
}

module.exports = MassaSeeder;
