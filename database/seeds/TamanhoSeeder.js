"use strict";

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class TamanhoSeeder {
    async run() {
        await Factory.get("tamanhos").create({
            name: "MÉDIA 25CM 4 FATIAS",
            price: 34.0,
            promotion_day_start: 0,
            promotion_day_end: 2,
        });
        await Factory.get("tamanhos").create({
            name: "GRANDE 30CM 6 FATIAS",
            price: 38.0,
            promotion_day_start: 1,
            promotion_day_end: 3,
        });
        await Factory.get("tamanhos").create({
            name: "GIGANTE 35CM 8 FATIAS",
            price: 48.0,
            promotion_day_start: 3,
            promotion_day_end: 4,
        });
        await Factory.get("tamanhos").create({
            name: "FAMÍLIA 40CM 12 FATIAS",
            price: 58.0,
            promotion_day_start: 4,
            promotion_day_end: 6,
        });
    }
}

module.exports = TamanhoSeeder;
