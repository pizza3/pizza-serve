"use strict";

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

Factory.blueprint("tamanhos", async (faker, i, data) => {
    return {
        name: data.name,
        price: data.price,
        promotion_day_start: data.promotion_day_start,
        promotion_day_end: data.promotion_day_end,
        score: parseInt(data.price * 0.15),
    };
});

Factory.blueprint("recheios", async (faker, i, data) => {
    return {
        name: data.name,
        description: data.description,
        price: data.price,
        score: parseInt(data.price * 0.15),
        promotion_day_start: data.promotion_day_start,
        promotion_day_end: data.promotion_day_end,
    };
});
Factory.blueprint("bordas", async (faker, i, data) => {
    return {
        name: data.name,
        price: data.price,
        score: parseInt(data.price * 0.2),
        promotion_day_start: data.promotion_day_start,
        promotion_day_end: data.promotion_day_end,
    };
});
Factory.blueprint("massas", async (faker, i, data) => {
    return {
        name: data.name,
        price: data.price,
        score: parseInt(data.price * 0.15),
        promotion_day_start: data.promotion_day_start,
        promotion_day_end: data.promotion_day_end,
    };
});
