"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Tamanho = use("App/Models/Tamanho");
const Massa = use("App/Models/Massa");
const Borda = use("App/Models/Borda");
const Recheio = use("App/Models/Recheio");

const Database = use("Database");

class PizzaController {
    async index({ request, response, view }) {}

    async create({ request, response, view }) {}

    async pedido({ request, response }) {
        let dayWeek = new Date().getDay();
        let scorePizza = 0;
        const dados = request.only([
            "tamanho_id",
            "massa_id",
            "borda_id",
            "recheio_id",
        ]);
        try {
            const tamanho = await Database.raw(
                `select * from tamanhos where id = ${dados.tamanho_id}`
            );
            const massa = await Database.raw(
                `select * from massas where id = ${dados.massa_id}`
            );
            const borda = await Database.raw(
                `select * from bordas where id = ${dados.borda_id}`
            );
            const recheio = await Database.raw(
                `select * from recheios where id = ${dados.recheio_id}`
            );

            if (
                tamanho.rows[0].promotion_day_start <= dayWeek &&
                tamanho.rows[0].promotion_day_end >= dayWeek
            ) {
                scorePizza += tamanho.rows[0].score;
            }
            if (
                massa.rows[0].promotion_day_start <= dayWeek &&
                massa.rows[0].promotion_day_end >= dayWeek
            ) {
                scorePizza += massa.rows[0].score;
            }
            if (
                borda.rows[0].promotion_day_start <= dayWeek &&
                borda.rows[0].promotion_day_end >= dayWeek
            ) {
                scorePizza += borda.rows[0].score;
            }
            if (
                recheio.rows[0].promotion_day_start <= dayWeek &&
                recheio.rows[0].promotion_day_end >= dayWeek
            ) {
                scorePizza += recheio.rows[0].score;
            }
            return response.status(200).json({
                message: `Parabéns você ganhou ${scorePizza}pt em benefícios.`,
            });
        } catch (err) {
            return response
                .status(400)
                .json({ message: "Não foi possivel encontrar os itens." });
        }
    }

    async store({ request, response }) {}

    /**
     * Display a single pizza.
     * GET pizzas/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async show({ params, request, response, view }) {}

    /**
     * Render a form to update an existing pizza.
     * GET pizzas/:id/edit
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async edit({ params, request, response, view }) {}

    /**
     * Update pizza details.
     * PUT or PATCH pizzas/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async update({ params, request, response }) {}

    /**
     * Delete a pizza with id.
     * DELETE pizzas/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async destroy({ params, request, response }) {}
}

module.exports = PizzaController;
