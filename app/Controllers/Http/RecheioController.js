"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Recheios = use("App/Models/Recheio");

class RecheioController {
    async index({ request, response }) {
        const dayWeek = new Date().getDay();
        const recheios = await Recheios.query().fetch();

        const recheiosDailyDeal = await Recheios.query()
            .where("promotion_day_start", "<=", dayWeek)
            .andWhere("promotion_day_end", ">=", dayWeek)
            .fetch();

        return response
            .status(200)
            .json({ dailyDeal: recheiosDailyDeal, all: recheios });
    }

    async create({ request, response, view }) {}

    async store({ request, response }) {}

    async show({ params, request, response, view }) {}

    async edit({ params, request, response, view }) {}

    async update({ params, request, response }) {}
    async destroy({ params, request, response }) {}
}

module.exports = RecheioController;
