"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Massas = use("App/Models/Massa");

class MassaController {
    async index({ request, response }) {
        const dayWeek = new Date().getDay();
        const massas = await Massas.query().fetch();
        const massasDailyDeal = await Massas.query()
            .where("promotion_day_start", "<=", dayWeek)
            .andWhere("promotion_day_end", ">=", dayWeek)
            .fetch();

        return response
            .status(200)
            .json({ dailyDeal: massasDailyDeal, all: massas });
    }

    async create({ request, response, view }) {}

    async store({ request, response }) {}
    async show({ params, request, response, view }) {}

    async edit({ params, request, response, view }) {}
    async update({ params, request, response }) {}
    async destroy({ params, request, response }) {}
}

module.exports = MassaController;
