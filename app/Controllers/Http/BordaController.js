"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Bordas = use("App/Models/Borda");

class BordaController {
    async index({ request, response }) {
        const dayWeek = new Date().getDay();
        const bordas = await Bordas.query().fetch();
        const bordasDailyDeal = await Bordas.query()
            .where("promotion_day_start", "<=", dayWeek)
            .andWhere("promotion_day_end", ">=", dayWeek)
            .fetch();

        return response
            .status(200)
            .json({ dailyDeal: bordasDailyDeal, all: bordas });
    }

    async create({ request, response, view }) {}

    async store({ request, response }) {}

    async show({ params, request, response, view }) {}

    async edit({ params, request, response, view }) {}

    async update({ params, request, response }) {}

    async destroy({ params, request, response }) {}
}

module.exports = BordaController;
