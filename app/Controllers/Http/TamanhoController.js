"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Tamanho = use("App/Models/Tamanho");

class TamanhoController {
    async index({ request, response }) {
        const dayWeek = new Date().getDay();
        const tamanhos = await Tamanho.query().fetch();

        const tamanhosDailyDeal = await Tamanho.query()
            .where("promotion_day_start", "<=", dayWeek)
            .andWhere("promotion_day_end", ">=", dayWeek)
            .fetch();

        return response
            .status(200)
            .json({ dailyDeal: tamanhosDailyDeal, all: tamanhos });
    }

    async create({ request, response, view }) {}

    async store({ request, response }) {}

    async show({ params, request, response, view }) {}
    async edit({ params, request, response, view }) {}
    async update({ params, request, response }) {}
    async destroy({ params, request, response }) {}
}

module.exports = TamanhoController;
